  $(document).ready(function(param) {
   
   var qsparam = new Array(2);
                var query = window.location.search.substring(1);
                var id = query.split('=');
   
   
   
	$.ajax({

  //Beim betätigen des Button "Kontaktn bearbeiten" werden die Daten mit der ausgwählten ID eingelesen. Das Adressbuch 
  //wechselt selbständig auf die Seite "KontaktBearbeiten" und befüllt die Felder.
  
  
  url: '/adressbuch/id/'+id[1],
  dataType: 'json',
  success: function (data, textStatus, jqXHR) {
    $("#id").val(data.id);	
	$("#vname").val(data.vname);
	$("#nname").val(data.nname);
	$("#adresse").val(data.adresse);
	$("#telefon").val(data.telefon);
	$("#mobilnummer").val(data.mobilnummer);
		$("#mail").val(data.mail);
		if(data.anrede == 'Herr'){
			$('#anrede1').prop('checked',true);
		}
		else{
			$('#anrede2').prop('checked',true);
		}
  }
});
});

  
  $(function() {
    $(".button").click(function() {
       changeKontakt();
       window.location.replace('/anzeigen.html');
	   return false;
    });
  });
  

	//Sobald die Daten geändert wurden, kann der Button "Eingaben speichern" betätigt werden. So werden 
	//die alten Eingaben überschrieben und der Kontakt ist aktualisiert
  
    function changeKontakt() {
        // get the form data
        var formData = {
			'id' : $('input[name=id]').val(),
			'anrede' : $("input[name=anrede]:checked").val(),
            'vname' : $('input[name=vname]').val(),
            'nname' : $('input[name=nname]').val(),
			'adresse' : $('input[name=adresse]').val(),			
            'telefon' : $('input[name=telefon]').val(),                
            'mobilnummer' : $('input[name=mobilnummer]').val(),
            'mail' : $('input[name=mail]').val(),
			'gruppe' : $("input[name=gruppe]:checked").val(),
    };
    // process the form
    //Wenn der Ajax-Befehl erfolgreich abgearbeitet wurde (success), kommt ein PopUp-Fenster mit 
    //"Kontakt wurde bearbeitet".
    $.ajax({
        type : 'PUT', // define the type of HTTP verb we want to use (POST for our form)
        contentType : 'application/json',
        url : '/kontakte/'+formData.id, // url where we want to POST
        data : JSON.stringify(formData), // data we want to POST
        success: function( data, textStatus, jQxhr ){
			alert("Kontakt wurde bearbeitet");
        //loadDataTable();
    }
	
    });
    
    }
