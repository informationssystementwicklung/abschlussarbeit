$(document).ready(function(param) {
   
   var qsparam = new Array(2);
                var query = window.location.search.substring(1);
                var id = query.split('=');
				$("#id1").val(id[1]);

				
	loadFreundeTable(id[1]);
	loadFamilieTable(id[1]);
	loadBekannteTable(id[1]);
});


$(function() {
    $(".addRelation").click(function() {
       addRelationship();
	   return false;
    });
  });
  
  function addRelationship(){
	var id1 = document.getElementById("id1").value;
	var id2 = document.getElementById("id2").value;
	var radios = document.getElementsByName('gruppe');
	var ralationTyp;
	//In einer Schleife werden alle RadioButtons mit dem Namen "gruppe" überprüft, ob diese gecheckt sind
	//wenn ja, wird der Wert des RB in einer Variable gespeichert
	for (var i = 0, length = radios.length; i < length; i++)
	{
	 if (radios[i].checked)
	 {
	  
	  ralationTyp=radios[i].value
	  break;
	 }
	}

	$.ajax({
        type : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url : '/kontakte/'+id1+'/add/'+ralationTyp+'/'+id2, // url where we want to POST
        success: function(result){
		alert("Beziehung wurde hinzugefügt.");
    }

    });  
  }
  
  function loadFreundeTable(id){
	 var table = $('#freundeTabelle').DataTable({
    destroy: true,
    "ajax": {
        "url": "/beziehung/freunde/"+id, //URL
        "dataSrc": "" 
    },
    "columns": [
        { "data": "id" },
        { "data": "vname" },
        { "data": "nname" },
        { "data": "anrede" },
        { "data": "adresse" },
        { "data": "telefon" },
        { "data": "mail" },
        { "data": "mobilnummer" },
        { "data": "gruppe" },
    ]
    });
  }
  
    function loadFamilieTable(id){
	  var table = $('#familienTabelle').DataTable({
    destroy: true,
    "ajax": {
        "url": "/beziehung/familie/"+id, //URL
        "dataSrc": "" 
    },
    "columns": [
        { "data": "id" },
        { "data": "vname" },
        { "data": "nname" },
        { "data": "anrede" },
        { "data": "adresse" },
        { "data": "telefon" },
        { "data": "mail" },
        { "data": "mobilnummer" },
        { "data": "gruppe" },
    ]
    });
  }
  
    function loadBekannteTable(id){
	  var table = $('#bekannteTabelle').DataTable({
    destroy: true,
    "ajax": {
        "url": "/beziehung/bekannte/"+id, //URL
        "dataSrc": "" 
    },
    "columns": [
        { "data": "id" },
        { "data": "vname" },
        { "data": "nname" },
        { "data": "anrede" },
        { "data": "adresse" },
        { "data": "telefon" },
        { "data": "mail" },
        { "data": "mobilnummer" },
        { "data": "gruppe" },
    ]
    });
  }
  
  