//Beim aufruf der Seite werde die Abgespeicherten Kontakte in die Tabelle geladen
$(document).ready(function() {
	//Rufe Funktion auf
    loadDataTable();
    
});
    function loadDataTable() {
	//Die Infos für die Tabelle werden über einen ajax Aufruf in ein Datenobjekt geladen
    var table = $('#adressbuchTabelle').DataTable({
    destroy: true,
    "ajax": {
        "url": "/adressbuch", //URL
        "dataSrc": "" 
    },
    "columns": [
        { "data": "id" },
        { "data": "vname" },
        { "data": "nname" },
        { "data": "anrede" },
        { "data": "adresse" },
        { "data": "telefon" },
        { "data": "mail" },
        { "data": "mobilnummer" },
        { "data": "gruppe" },
		{		//Um einen Kontakt aus der Tabelle heraus zu löschen, wird in der Zeile für den Kontakt ein Button hinzugefügt, welcher die ID des Kontaktes hat
                 sortable: false,
                 "render": function ( data, type, table, meta ) {
                     var buttonID = table.id;
                     return '<input id="'+buttonID+'" type="submit" class="buttonLoeschen" value="Löschen">';
                 }
             },
		{	//Um einen Kontakt aus der Tabelle heraus zu ändern, wird in der Zeile für den Kontakt ein Button hinzugefügt, welcher die ID des Kontaktes hat

                 sortable: false,
                 "render": function ( data, type, table, meta ) {
                     var buttonID = table.id;
                     return '<input id="'+buttonID+'" type="submit" class="buttonBearbeiten" value="Kontakt bearbeiten">';
                 }
             },
		{	//Um eine nue Beziehung zwischen den Kontakten herzustellen kann die eigene ID mit der ID von jemand andrern verknüpft werden

                 sortable: false,
                 "render": function ( data, type, table, meta ) {
                     var buttonID = table.id;
                     return '<input id="'+buttonID+'" type="submit" class="beziehungHinzufügen" value="Beziehung hinzufügen">';
                 }
             },
    ]
    });
}


//Event wird aufgerufen, wenn auf den button mit der Klasse .button geklickt wird.
$(function() {
    $(".button").click(function() {
       createKontakt();
	   return false;
    });
  });
    function createKontakt() {
        // Datenobjekt wird aus den Inputfeldern erstellt.
        var formData = {
            'vname' : $('input[name=vname]').val(),
            'nname' : $('input[name=nname]').val(),
            'anrede' : $("input[name=anrede]:checked").val(),
            'adresse' : $('input[name=adresse]').val(),                
            'telefon' : $('input[name=telefon]').val(),
            'mail' : $('input[name=mail]').val(),
            'mobilnummer' : $('input[name=mobilnummer]').val(),
            'gruppe' : $("input[name=gruppe]:checked").val(),
    };
    // ajax Aufruf an den Server zum erstellen des neuen Kontaktes
    $.ajax({
        type : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        contentType : 'application/json',
        url : '/kontakterstellen', // url where we want to POST
        data : JSON.stringify(formData), // data we want to POST
        success: function( data, textStatus, jQxhr ){
			alert("Kontakt wurde gespeichert");
    }
    });

    }
	//Die Mehtoden wird mit der Kontakt ID als Parameter aufgerufen
	function deleteMeinKontakt(param){
		$.ajax({
        type : 'DELETE', // define the type of HTTP verb we want to use (POST for our form)
        url : '/kontaktloeschen/'+param, // an die URL wird die ID angehängt, damit der Server weiss, welche Kontakt gelöscht wird
        success: function(result){
			setTimeout(function(){
           location.reload(); // Seite wird nach kurzer Zeit neu geladen
      }, 1000); 
		alert("Kontakt wurde geloescht!");
    }

    });
	}
function addBeziehung(id){
	window.location.replace('/BeziehungHinzufuegen.html?id=' + id);
	
}
  
  $(document).on('click', '.buttonLoeschen', function(event){ 
     deleteMeinKontakt(event.target.id);
	//Die ID von jedem Button entspricht der ID des Kontaktes
	 return false;
	 location.reload();
});

$(document).on('click', '.beziehungHinzufügen', function(event){ 
     addBeziehung(event.target.id);
	//Die ID von jedem Button entspricht der ID des Kontaktes
	 return false;
	 location.reload();
});


$(document).on('click', '.buttonBearbeiten', function(event){ 
     //deleteMeinKontakt(event.target.id);
	 editKontakt(event.target.id);
	 return false;
});
//Die Seite zum bearbeiten wird aufgerufen mit der ID als Parameter in der URL
function editKontakt(id){
	window.location.replace('/KontaktBearbeiten.html?id=' + id);
}
