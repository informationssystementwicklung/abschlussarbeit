
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.junit.*;

import gp.aalen.adressbuch.Kontakt;
import gp.aalen.adressbuch.KontaktController;
import gp.aalen.adressbuch.KontaktService;

public class JUnitTestService {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	Kontakt k;
	KontaktController kc;
	KontaktService ks;
	
	/*Variablen und Inhalte initialisieren*/
	@Before
	public void init() {
		k = new Kontakt("Herr", "Daniel", "Schlechte", "Brunnenstr. 25", "+4915121231066", 
				"0711123456789", "daniel.schlechte@studmail.htw.aalen", "Bekannter");
		kc = new KontaktController();
		ks = new KontaktService();
	}
	
	/*Testmethoden*/
	@Test
	public void getAllKontakteOfnname() {
		List <Kontakt> name = ks.getAllKontakteOfnname("Schlechte");
		assertEquals(k, name.get(0));
	}
	
	@Test
	public void getKontakteById() {
		Kontakt name = ks.getKontakteById(1);
		assertEquals(k, name);
	}
	
	

}
