package gp.aalen.adressbuch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class KontaktService {
	
	private static final Logger logger = LogManager.getLogger(); //Log4J Test 
	
	@Autowired
	private KontaktRepository kontaktRepository;
	
	/**Kontakte auflisten*/
	public List<Kontakt> getAdressbuchList() {
		ArrayList<Kontakt> myList = new ArrayList<>();
		Iterator<Kontakt> it = kontaktRepository.findAll().iterator(); 
		while(it.hasNext())
		myList.add(it.next());
		return myList;}

	/**Kontakt hinzufügen und speichern inklusive das implementieren des loggers*/
	public void addKontakt(@RequestBody Kontakt kontakt) { 
		kontaktRepository.save(kontakt);
		logger.info("Kontakt gespeichert: " + kontakt.getId()+" " + kontakt.getnname()+ " " + kontakt.getvname());
	}

	/**Kontakt ändern und speichern inklusive implementieren des loggers*/
	public void updateKontakt(@PathVariable long id, @RequestBody Kontakt kontakt) { 
		kontaktRepository.save(kontakt);
		logger.info("Kontakt gespeichert: " + kontakt.getId()+" " + kontakt.getnname()+ " " + kontakt.getvname());

	}
	
	/**Kontakt löschen inklusive implementieren des loggers*/
	public void deleteKontakt(@PathVariable int id) {
		kontaktRepository.delete(id); 
		logger.info("Kontakt geloescht");

	}	
	
	/**Kontakte nach nnamen ausgeben inklusive implementieren des Loggers*/
	public List<Kontakt> getAllKontakteOfnname(String nname) {
		logger.info("Suchfunktion ausgefuehrt!" + nname);
		return kontaktRepository.findBynname(nname); 
	}	

	/**Beziehungen nach Freunde auflisten*/
	public List<Kontakt> getAllKontakteOfFreundeId(int id) {
		Kontakt k = kontaktRepository.findOne(id);
		return k.getFreunde();
	}	
	
	/**Beziehungen nach bekannte auflisten*/
	public List<Kontakt> getAllKontakteOfBekannteId(int id) {
		Kontakt k = kontaktRepository.findOne(id);
		return k.getBekannte();
	}
	
	/**Beziehungen auflisten*/
	public List<Kontakt> getAllKontakteOfFamilieId(int id) {
		Kontakt k = kontaktRepository.findOne(id);
		return k.getFamilie();
	}
	
	/**Kontakte nach ID ausgeben*/
	public Kontakt getKontakteById(int id) {
		return kontaktRepository.findById(id);		
	}	
	
	/**Beziehungen nach Freunde finden*/
	public List <Kontakt> getFreunde(int id) {
		Kontakt k = kontaktRepository.findOne(id);
		return k.getFreunde();
	}
	
	/**Beziehungen nach bekannte findien*/
	public List <Kontakt> getBekannte(int id) {
		Kontakt k = kontaktRepository.findOne(id);
		return k.getBekannte();
	}
	
	/**Beziehungen nach Familie finden*/
	public List <Kontakt> getFamilie(int id) {
		Kontakt k = kontaktRepository.findOne(id);
		return k.getFamilie();
	}
	
	/**Beziehungen erstellen */
	public void addFreundeBeziehung(int id1, int id2) {
		Kontakt k1 = kontaktRepository.findById(id1);
		Kontakt k2 = kontaktRepository.findById(id2);
		List <Kontakt> alteFreunde = k1.getFreunde();
		Iterator <Kontakt> it = alteFreunde.iterator();
		boolean bekannt = false;
		while(it.hasNext()) {
			Kontakt alterFreund = it.next();
			if(alterFreund.getId() == id2) {
			bekannt = true;
			}
		}
		if(bekannt == false) {
			k1.addFreundeinListe(k2);
			kontaktRepository.save(k1);
		}
	}
	
	/**Beziehungen erstellen*/
	public void addBekannteBeziehung(int id1, int id2) {
		Kontakt k1 = kontaktRepository.findOne(id1);
		Kontakt k2 = kontaktRepository.findOne(id2);
		List <Kontakt> alteBekannte = k1.getFreunde();
		Iterator <Kontakt> it = alteBekannte.iterator();
		boolean bekannt = false;
		while(it.hasNext()) {
			Kontakt alterBekannter = it.next();
			if(alterBekannter.getId() == id2) {
			bekannt = true;
			}
		}
		if(bekannt == false) {
			k1.addBekannteinListe(k2);
			kontaktRepository.save(k1);
		}
	}
		
	/**Beziehungen erstellen*/
		public void addFamilieBeziehung(int id1, int id2) {
			Kontakt k1 = kontaktRepository.findOne(id1);
			Kontakt k2 = kontaktRepository.findOne(id2);
			List <Kontakt> alteFamilie = k1.getFreunde();
			Iterator <Kontakt> it = alteFamilie.iterator();
			boolean bekannt = false;
			while(it.hasNext()) {
				Kontakt alterFamilie = it.next();
				if(alterFamilie.getId() == id2) {
				bekannt = true;
				}
			}
			if(bekannt == false) {
				k1.addFamilieinListe(k2);
				kontaktRepository.save(k1);
			}
		}
		
	/* Aufheben von Beziehunge
		public void deleteBeziehung(int id, int id2) {
		List <Kontakt> altebeziehung = k.getBeziehung();
		List <Kontakt> neueBeziehung = new ArrayList <Kontakt>();
		Iterator <Kontakt> it = alteBeziehung.iterator();
		while(it.hasNext()) {
			Kontakt alterBekannter = it.next();
			if(alteBeziehung.getId != id2) {
			neueBeziehung.add(alteBeziehung);
		}
	*/
	
}

