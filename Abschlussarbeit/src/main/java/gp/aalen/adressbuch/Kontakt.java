package gp.aalen.adressbuch;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;



import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Kontakt {


	/**Variablen initialisieren*/
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	private int id;
	private String vname;
	private String nname;
	private String anrede;
	private String adresse;
	private String telefon;
	private String mail;
	private String mobilnummer;
	private String gruppe;
	
/*	@ManyToMany (mappedBy="gruppe")
	private List<Kontakt> gruppeLinks; */
	

	public Kontakt() {
	}
	
//	 @ManyToMany//(mappedBy="Gruppe")
//	 @JsonIgnore 
//	 private List<Kontakt> kontaktGruppe; //@Column(name ="published_date") 
	
	/**Herstellen der ManyToMany Beziehungen*/
	 @ManyToMany
	 private List<Kontakt> familie;
	 
	@ManyToMany
	 private List<Kontakt> freunde;
	 
	 @ManyToMany
	 private List<Kontakt> bekannte;

	/**Rückgabewerte definieren*/
	public Kontakt(String anrede, String vname, String nname, String adresse, String telefon, String mobilnummer,
			String mail, String gruppe) {

		super();
		this.anrede = anrede;
		this.vname = vname;
		this.nname = nname;
		this.adresse = adresse;
		this.telefon = telefon;
		this.mobilnummer = mobilnummer;
		this.mail = mail;
		this.gruppe = gruppe;
	}

	/** Methoden zum setzen und rückgeben der Werte (Getter- und
	 Setter-Methoden) */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAnrede() {
		return anrede;
	}

	public void setAnrede(String anrede) {
		this.anrede = anrede;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getmail() {
		return mail;
	}

	public void setmail(String mail) {
		this.mail = mail;
	}

	public String getmobilnummer() {
		return mobilnummer;
	}

	public void setmobilnummer(String mobilnummer) {
		this.mobilnummer = mobilnummer;
	}

	public String getvname() {
		return vname;
	}

	public void setvname(String vname) {
		this.vname = vname;
	}

	public String getnname() {
		return nname;
	}

	public void setnname(String nname) {
		this.nname = nname;
	}
	public String getGruppe() {
		return gruppe;
	}

	public void setGruppe(String gruppe) {
		this.gruppe = gruppe;
	}

	public List<Kontakt> getFamilie() {
		return familie;
	}

	public void setFamilie(List<Kontakt> familie) {
		this.familie = familie;
	}

	public List<Kontakt>getFreunde() {
		return freunde;
	}

	public void setFreunde(List<Kontakt> freunde) {
		this.freunde = freunde;
	}
	
	public List<Kontakt> getBekannte() {
		return bekannte;
	}

	public void addBekannteinListe(Kontakt k) {
			this.bekannte.add(k);
	}
	
	public void addFreundeinListe(Kontakt k) {
		this.freunde.add(k);
	}
	
	public void addFamilieinListe(Kontakt k) {
		this.familie.add(k);
	}

}
