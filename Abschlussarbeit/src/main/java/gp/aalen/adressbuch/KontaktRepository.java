package gp.aalen.adressbuch;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface KontaktRepository extends CrudRepository<Kontakt, Integer> {
	
	/**Implementieren der Suchfunktion*/
	public List<Kontakt> findBynname(String nname); //Kontakte nach Namen finden
	public Kontakt findById(int id); //Kontakte nach ID finden
	
	
}