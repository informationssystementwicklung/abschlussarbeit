package gp.aalen.adressbuch;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gp.aalen.adressbuch.Kontakt;
import gp.aalen.adressbuch.KontaktService;

/**Anlegen der Rest Schnittstelle*/
@RestController
public class KontaktController {
	
	@Autowired
	KontaktService kontaktService; 
	
	/**Erstellen der Mappings inkl. der PUT, DELETE und POST Variablen*/
	@RequestMapping("/adressbuch") 
	public List<Kontakt> getKontaktList(){
		return kontaktService.getAdressbuchList();
	}

	@RequestMapping("/adressbuch/{nname}")  

	public List<Kontakt> getAllKontakteOfnname(@PathVariable String nname){
		return kontaktService.getAllKontakteOfnname(nname);
	}	

	@RequestMapping("/beziehung/freunde/{id}") 
	public List<Kontakt> getAllKontakteOfFreundeId(@PathVariable int id){
		return kontaktService.getAllKontakteOfFreundeId(id);
	}
	
	@RequestMapping("/beziehung/bekannte/{id}") 
	public List<Kontakt> getAllKontakteOfBekannteId(@PathVariable int id){
		return kontaktService.getAllKontakteOfBekannteId(id);
	}
	
	@RequestMapping("/beziehung/familie/{id}") 
	public List<Kontakt> getAllKontakteOfFamilieId(@PathVariable int id){
		return kontaktService.getAllKontakteOfFamilieId(id);
	}
	
	@RequestMapping("/adressbuch/id/{id}") //Mapping Adressbuch ausgeben nach ID
	public Kontakt getAllKontakteOfid(@PathVariable int id){
		return kontaktService.getKontakteById(id);
	}		

	@RequestMapping(method=RequestMethod.POST, value="/kontakterstellen")  //Mapping Erstellen eines Kontakts
	public void addKontakt(@RequestBody Kontakt kontakt) {
		kontaktService.addKontakt(kontakt); 
		}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/kontaktloeschen/{id}") //Mapping Kontakt löschen 
	public void deleteKontakt(@PathVariable int id) {
		kontaktService.deleteKontakt(id); 
		}
	
	@RequestMapping(method=RequestMethod.PUT, value="/kontakte/{id}") //Mapping Kontakt updaten
	public void updateKontakt(@PathVariable long id, @RequestBody Kontakt kontakt) {
		kontaktService.updateKontakt(id, kontakt); 
		}
	
	@RequestMapping(method=RequestMethod.POST, value="/kontakte/{id1}/add/freunde/{id2}")
	public void addFreundeBeziehung(@PathVariable int id1, @PathVariable int id2) {
		kontaktService.addFreundeBeziehung(id1, id2);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/kontakte/{id1}/add/bekannte/{id2}")
	public void addBekannteBeziehung(@PathVariable int id1, @PathVariable int id2) {
		kontaktService.addBekannteBeziehung(id1, id2);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/kontakte/{id1}/add/familie/{id2}")
	public void addFamilieBeziehung(@PathVariable int id1, @PathVariable int id2) {
		kontaktService.addFamilieBeziehung(id1, id2);
	}
}

