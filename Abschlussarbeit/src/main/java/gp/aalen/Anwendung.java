package gp.aalen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Anwendung {

	public static void main(String[] args) {
		/*Starten der Spring Applikation*/
		SpringApplication.run(Anwendung.class, args);
	}

}
