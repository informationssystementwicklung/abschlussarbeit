+# README #
+
+### Wozu dient dieses Archiv? ###
+
+Version 1.0
+In unserem Archiv geht es um eine komplette Kontaktverwaltung.
+
+
+### Wie wurde die Kontaktverwaltung eingerichtet? ###
Add a comment to this line
+HTML wurden 3 Seiten erstellt. Für das CSS wurde eine Index-Datei erstellt.
+Als Datenbank wurde Maria DB verwendet.
+Für die Anwendungen wurden mit Docker, Container erstellt um mit der Datenbank einfach und schnell kommunizieren zu können. 
+Als Datenbankanbindung wurde eine application.properties angelegt und mit build.gradle die Datenbankanbindung angepasst.
+Für die grafische Oberfläche wurde Kitematic verwendet.
+Über das Mapping kann direkt auf bestimmte Seiten verwiesen werden.
+Ein Logging wurde integriert um Aktivitäten zu protokollieren. 
+JavaSkript wurde mit JQuery framework erstellt.
+
+
+# Aufbau
+Die Kontaktverwaltung wurde auf 3 Seiten aufgeteilt. 
+Die Startseite beinhaltet eine Suchleiste um einen bestimmten Kontakt schnell zu finden.
+Des weiteren wurden hier zwei Buttons eingefügt um auf die anderen internen Seiten zu verweisen (Kontakt erstellen und Kontaktliste anzeigen).
+Die Seite Kontakt erstellen beinhaltet ein Formular welches alle notwendigen Daten des Neu-Kunden erfasst und diese auf die Datenbank abspeichert.
+Die Daten Anrede und Gruppe werden über Radio-Buttons abgefragt, Vorname, Nachname, Telefon, Mobilnummer und E-mail über normale eingabefelder.
+Zusätzlich wurde hier wieder ein Button integriert welcher auf die Startseite zurück verweist und ein Button um die Daten abzuspeichern.
+Auf Kontaktliste anzeigen wurde wieder eine Suchleiste eingepflegt, ein Button welcher wieder auf die Startseite verweist und einer um wieder zu Kontakt erstellen verweist.
+Des weiteren wurde hier eine Tabelle itegriert welche alle vorhandenen Kontakte anzeigt und diese auch direkt über die Tabelle geändert oder gelöscht werden können.
+Durch diesen Aufbau kann die Kontakverwaltung problemlos gepflegt werden.
+
+
+### Datenbankkonfiguration ###
+
+Als Datenbank wurde Maria DB verwendet.
+
+# General 
+BITNAMI_APP_NAME:			kontaktedb
+MARIADB_DATABASE:			kontaktedb
+MARIADB_ROOT_PASSWORT:		test123!
+MARIA_ROOT_USER:			root
+
+# Hostname/Ports			
+published IP_Port:		localhost/3306
+
+
+### Team ###
+
+Oliver Sauer		66626
+Oliver Haager		66829
+Christian Rink		66935
+Daniel Schlechte	66728
+Patrick Schenk		66784
+